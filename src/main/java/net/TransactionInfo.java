package net;

import java.util.concurrent.atomic.AtomicLong;

public class TransactionInfo {

    private static AtomicLong transactionNumber = new AtomicLong();
    private static boolean isAutoCommit = false;

    public static void setIsAutoCommit(boolean isAutoCommit) {
        TransactionInfo.isAutoCommit = isAutoCommit;
    }

    public static Long getTransactionNumber() {
        return isAutoCommit ? transactionNumber.incrementAndGet() : transactionNumber.get();
    }

    public static void increment() {
        transactionNumber.incrementAndGet();
    }

}
