package net.support;

import net.QueryCount;

/**
 */
public interface QueryCountLogEntryCreator {

    String getLogMessage(String datasourceName, QueryCount queryCount);
    String getLogMessageAsJson(String datasourceName, QueryCount queryCount);
}
