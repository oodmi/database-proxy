package net.proxy;

/**
 * Provide a method to unwrap the original jdbc object from proxy object.
 * <p>
 * <p>Proxy object created by {@link JdbcProxyFactory} implements this interface.
 *
 * @author Tadaya Tsuyukubo
 * @see JdbcProxyFactory
 * @see net.jdk.ConnectionInvocationHandler
 * @see net.jdk.StatementInvocationHandler
 * @see net.jdk.PreparedStatementInvocationHandler
 * @see net.jdk.CallableStatementInvocationHandler
 * @see net.jdk.ResultSetInvocationHandler
 */
public interface ProxyJdbcObject {

    /**
     * Method to return wrapped source object(Connection, Statement, PreparedStatement, CallableStatement).
     *
     * @return source object
     */
    Object getTarget();
}
