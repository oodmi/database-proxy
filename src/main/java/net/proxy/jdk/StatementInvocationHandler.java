package net.proxy.jdk;

import net.listener.QueryExecutionListener;
import net.proxy.InterceptorHolder;
import net.proxy.JdbcProxyFactory;
import net.proxy.StatementProxyLogic;
import net.transform.QueryTransformer;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.sql.Statement;

/**
 * Proxy InvocationHandler for {@link Statement}.
 */
public class StatementInvocationHandler implements InvocationHandler {

    private StatementProxyLogic delegate;

    public StatementInvocationHandler(Statement stmt) {
        final InterceptorHolder interceptors = new InterceptorHolder(QueryExecutionListener.DEFAULT, QueryTransformer.DEFAULT);
        delegate = new StatementProxyLogic(stmt, interceptors, "", JdbcProxyFactory.DEFAULT);
    }

    public StatementInvocationHandler(
            Statement stmt, InterceptorHolder interceptorHolder, String dataSourceName, JdbcProxyFactory jdbcProxyFactory) {
        delegate = new StatementProxyLogic(stmt, interceptorHolder, dataSourceName, jdbcProxyFactory);
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return delegate.invoke(method, args);
    }
}
