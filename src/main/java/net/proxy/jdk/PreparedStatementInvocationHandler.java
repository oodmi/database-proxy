package net.proxy.jdk;

import net.listener.QueryExecutionListener;
import net.proxy.InterceptorHolder;
import net.proxy.JdbcProxyFactory;
import net.proxy.PreparedStatementProxyLogic;
import net.transform.QueryTransformer;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;

/**
 * Proxy InvocationHandler for {@link PreparedStatement}.
 */
public class PreparedStatementInvocationHandler implements InvocationHandler {

    private PreparedStatementProxyLogic delegate;

    public PreparedStatementInvocationHandler(PreparedStatement ps, String query) {
        delegate = new PreparedStatementProxyLogic(ps, query, new InterceptorHolder(QueryExecutionListener.DEFAULT, QueryTransformer.DEFAULT), "", JdbcProxyFactory.DEFAULT);
    }

    public PreparedStatementInvocationHandler(
            PreparedStatement ps, String query, InterceptorHolder interceptorHolder, String dataSourceName, JdbcProxyFactory jdbcProxyFactory) {
        delegate = new PreparedStatementProxyLogic(ps, query, interceptorHolder, dataSourceName, jdbcProxyFactory);
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return delegate.invoke(method, args);
    }

}
