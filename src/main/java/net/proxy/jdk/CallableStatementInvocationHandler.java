package net.proxy.jdk;

import net.proxy.InterceptorHolder;
import net.proxy.JdbcProxyFactory;
import net.proxy.PreparedStatementProxyLogic;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.sql.CallableStatement;

/**
 * Proxy InvocationHandler for {@link CallableStatement}.
 */
public class CallableStatementInvocationHandler implements InvocationHandler {

    private PreparedStatementProxyLogic delegate;

    public CallableStatementInvocationHandler() {
    }

    public CallableStatementInvocationHandler(
            CallableStatement cs, String query, InterceptorHolder interceptorHolder, String dataSourceName, JdbcProxyFactory jdbcProxyFactory) {
        delegate = new PreparedStatementProxyLogic(cs, query, interceptorHolder, dataSourceName, jdbcProxyFactory);
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return delegate.invoke(method, args);
    }

}
