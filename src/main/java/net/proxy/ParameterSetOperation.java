package net.proxy;

import java.lang.reflect.Method;

public class ParameterSetOperation {

    private Method method;
    private Object[] args;

    public ParameterSetOperation() {
    }

    public ParameterSetOperation(Method method, Object[] args) {
        this.method = method;
        this.args = args;
    }

    /**
     * Check the given operation is {@link java.sql.CallableStatement#registerOutParameter} method by method name.
     *
     * @param operation a parameter set operation
     * @return true if it is a {@code registerOutParameter} operation
     * @since 1.4
     */
    public static boolean isRegisterOutParameterOperation(ParameterSetOperation operation) {
        String methodName = operation.getMethod().getName();
        return StatementMethodNames.PARAMETER_METHOD_REGISTER_OUT_PARAMETER.equals(methodName);
    }

    /**
     * Check the given operation is {@code setNull} method by method name.
     *
     * @param operation a parameter set operation
     * @return true if it is a {@code setNull} operation
     * @since 1.4
     */
    public static boolean isSetNullParameterOperation(ParameterSetOperation operation) {
        String methodName = operation.getMethod().getName();
        return StatementMethodNames.PARAMETER_METHOD_SET_NULL.equals(methodName);
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Object[] getArgs() {
        return args;
    }

    public void setArgs(Object[] args) {
        this.args = args;
    }
}
