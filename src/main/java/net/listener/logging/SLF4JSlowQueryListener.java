package net.listener.logging;

import net.summary.Summary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 * Log slow query using SLF4j.
 */
public class SLF4JSlowQueryListener extends AbstractSlowQueryLoggingListener {

    protected Logger logger = LoggerFactory.getLogger(SLF4JSlowQueryListener.class);

    public SLF4JSlowQueryListener() {
    }

    public SLF4JSlowQueryListener(long threshold, TimeUnit thresholdTimeUnit) {
        this.threshold = threshold;
        this.thresholdTimeUnit = thresholdTimeUnit;
    }

    @Override
    protected void writeLog(String message) {
        logger.debug(message);
        Summary.getInstance().add(message);
    }

}
