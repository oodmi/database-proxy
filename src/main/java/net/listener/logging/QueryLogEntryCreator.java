package net.listener.logging;

import net.ExecutionInfo;
import net.QueryInfo;

import java.util.List;

/**
 * Generate logging entry.
 */
public interface QueryLogEntryCreator {

    String getLogEntry(ExecutionInfo execInfo, List<QueryInfo> queryInfoList, boolean writeDataSourceName);

}
