package net.listener.logging;

import net.ExecutionInfo;
import net.QueryInfo;
import net.information.QueryInformation;
import net.information.QueryInformationHolder;
import net.listener.SlowQueryListener;

import java.util.List;

/**
 * Abstract class to log slow query.
 * <p>
 * This class delegates actual log writing to subclasses.
 */
public abstract class AbstractSlowQueryLoggingListener extends SlowQueryListener {

    protected boolean writeDataSourceName = true;
    protected QueryLogEntryCreator queryLogEntryCreator = new DefaultJsonQueryLogEntryCreator();
    protected String prefix;

    @Override
    protected void onSlowQuery(ExecutionInfo execInfo, List<QueryInfo> queryInfoList, long startTimeInMills) {
        String entry = this.queryLogEntryCreator.getLogEntry(execInfo, queryInfoList, this.writeDataSourceName);
        if (this.prefix != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(this.prefix);
            sb.append(entry);
            entry = sb.toString();
        }
        QueryInformationHolder.put(execInfo.getTransactionId(), new QueryInformation(execInfo, queryInfoList));
        writeLog(entry);
    }

    protected abstract void writeLog(String message);

    public QueryLogEntryCreator getQueryLogEntryCreator() {
        return queryLogEntryCreator;
    }

    public void setQueryLogEntryCreator(QueryLogEntryCreator queryLogEntryCreator) {
        this.queryLogEntryCreator = queryLogEntryCreator;
    }

}
