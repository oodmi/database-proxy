package net.listener.logging;

import net.summary.Summary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Log executed query information using SLF4J.
 */
public class SLF4JQueryLoggingListener extends AbstractQueryLoggingListener {
    protected Logger logger = LoggerFactory.getLogger(SLF4JQueryLoggingListener.class);

    @Override
    protected void writeLog(String message) {
        logger.debug(message);
        Summary.getInstance().add(message);
    }
}
