package net.listener;

import net.ExecutionInfo;
import net.QueryInfo;

import java.util.List;

/**
 * No operation implementation of {@link QueryExecutionListener}
 */
public class NoOpQueryExecutionListener implements QueryExecutionListener {

    @Override
    public void beforeQuery(ExecutionInfo execInfo, List<QueryInfo> queryInfoList) {
        // do nothing
    }

    @Override
    public void afterQuery(ExecutionInfo execInfo, List<QueryInfo> queryInfoList) {
        // do nothing
    }
}
