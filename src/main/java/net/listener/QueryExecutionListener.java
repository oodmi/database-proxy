package net.listener;


import net.ExecutionInfo;
import net.QueryInfo;

import java.util.List;

/**
 * Listener interface. Inject the implementation to proxy handler interceptors.
 */
public interface QueryExecutionListener {

    static QueryExecutionListener DEFAULT = new NoOpQueryExecutionListener();

    void beforeQuery(ExecutionInfo execInfo, List<QueryInfo> queryInfoList);

    void afterQuery(ExecutionInfo execInfo, List<QueryInfo> queryInfoList);
}
