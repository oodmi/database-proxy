package net.summary;

import java.util.ArrayList;
import java.util.List;

public class Summary {
    private static Summary summary = new Summary();

    private List<Object> list;

    private Summary() {
        list = new ArrayList<Object>();
    }

    public static Summary getInstance() {
        return summary;
    }

    public void add(Object o) {
        list.add(o);
    }

    public List<Object> getList() {
        return list;
    }
}
