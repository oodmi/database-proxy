package net.information;

import net.ExecutionInfo;
import net.QueryInfo;

import java.util.List;

public class QueryInformation {
    private ExecutionInfo execInfo;
    private List<QueryInfo> queryInfoList;

    public QueryInformation() {
    }

    public QueryInformation(ExecutionInfo execInfo, List<QueryInfo> queryInfoList) {
        this.execInfo = execInfo;
        this.queryInfoList = queryInfoList;
    }

    public ExecutionInfo getExecInfo() {
        return execInfo;
    }

    public void setExecInfo(ExecutionInfo execInfo) {
        this.execInfo = execInfo;
    }

    public List<QueryInfo> getQueryInfoList() {
        return queryInfoList;
    }

    public void setQueryInfoList(List<QueryInfo> queryInfoList) {
        this.queryInfoList = queryInfoList;
    }
}
