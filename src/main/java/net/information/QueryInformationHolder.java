package net.information;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QueryInformationHolder {

    private static ThreadLocal<Map<Long, List<QueryInformation>>> queryInfoMapHolder = new ThreadLocal<Map<Long, List<QueryInformation>>>() {
        @Override
        protected Map<Long, List<QueryInformation>> initialValue() {
            return new HashMap<Long, List<QueryInformation>>();
        }
    };

    public static List<QueryInformation> get(Long tid) {
        final Map<Long, List<QueryInformation>> map = queryInfoMapHolder.get();
        return map.get(tid);
    }

    public static void put(Long tid, QueryInformation info) {
        List<QueryInformation> sortedSet = queryInfoMapHolder.get().get(tid);
        if (sortedSet == null) {
            queryInfoMapHolder.get().put(tid, new ArrayList<QueryInformation>());
        }
        queryInfoMapHolder.get().get(tid).add(info);
    }

    public static void clear() {
        queryInfoMapHolder.get().clear();
    }
}
