package net.transform;

/**
 * No operation implementation of {@link ParameterTransformer}.
 */
public class NoOpParameterTransformer implements ParameterTransformer {

    public void transformParameters(ParameterReplacer replacer, TransformInfo transformInfo) {
        // do nothing
    }
}
