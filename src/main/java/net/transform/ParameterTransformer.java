package net.transform;

/**
 * @author Tadaya Tsuyukubo
 */
public interface ParameterTransformer {

    static ParameterTransformer DEFAULT = new NoOpParameterTransformer();

    void transformParameters(ParameterReplacer replacer, TransformInfo transformInfo);
}
