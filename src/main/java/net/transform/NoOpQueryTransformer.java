package net.transform;

/**
 * No operation implementation of {@link QueryTransformer}.
 */
public class NoOpQueryTransformer implements QueryTransformer {
    public String transformQuery(TransformInfo transformInfo) {
        return transformInfo.getQuery();
    }
}
