package net.transform;

/**
 * Interceptor that can transform the query statement.
 */
public interface QueryTransformer {

    static QueryTransformer DEFAULT = new NoOpQueryTransformer();

    String transformQuery(TransformInfo transformInfo);
}
