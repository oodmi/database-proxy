package net;

public enum QueryType {
    SELECT, INSERT, UPDATE, DELETE, OTHER
}